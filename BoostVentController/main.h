/*
 * main.h
 *
 * Created: 8/18/2021 10:47:56 AM
 *  Author: thogan
 */ 

#ifndef MAIN_H_
void timer0_pwm_disable();
void timer0_pwm_enable();
void set_fan_speed(uint8_t speed);

#define MAIN_H_
#endif /* MAIN_H_ */