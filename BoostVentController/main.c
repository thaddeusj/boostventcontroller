/*
 * BoostVentController.c
 *
 * Created: 8/16/2021 8:29:07 PM
 * Author : thogan
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/sleep.h>
#include "main.h"

#define FAN_FULL_POINT 245
#define FAN_OFF_POINT 30
#define TCCR0A_ENABLED_VALUE (2<<COM0A0 | 3<<WGM00)
#define TCCR0B_ENABLED_VALUE (0<<WGM02 | 1<<CS00)
#define TIMER1_TICKS 130 /* For 60Hz interrupts */

volatile uint8_t current_speed;

/* Timer 1 interrupts
 * Poll the ADC for a new value and update fan speed */
ISR(TIM1_COMPA_vect) {
	/* If conversion complete */
	if (!(ADCSRA & (1<<ADSC))) {
		uint8_t new_speed = ADCH;
		uint8_t window_left = 0;
		uint8_t window_right = 255;
		
		if (current_speed >= 2) { window_left = current_speed - 2; }
		if (current_speed <= 253) { window_right = current_speed + 2; }
		
		/* To avoid wobbling between two settings, ensure the
		 * new value is at least 2 more or less than current_speed. */
		if (new_speed < window_left || new_speed > window_right) {
			set_fan_speed(ADCH);
		}
		
		ADCSRA |= 1<<ADSC; /* Start another conversion */
	}
}

/* Disable timer0 pwm for fan control.
 * Compare match output off (TCCR0A:COM0*)
 * Clock select off (TCCR0B:CS*)
 */
void timer0_pwm_disable() {
	TCCR0A = 0;
	TCCR0B = 0;
}

/* Disable timer0 pwm for fan control.
 * Compare match clear OC0A on match (TCCR0A:COM0A*)
 * Waveform generation mode fast pwm (TCCR0A:WGM00/01)
 * Clock select clkIO (TCCR0B:CS*)
 *
 * Read control registers first and only set if timer
 * is not already configured.
 */
void timer0_pwm_enable() {
	if (TCCR0A != TCCR0A_ENABLED_VALUE
	 || TCCR0B != TCCR0B_ENABLED_VALUE) {
		TCCR0A = TCCR0A_ENABLED_VALUE;
		TCCR0B = TCCR0B_ENABLED_VALUE;
	 }
}

/* Set fan speed.
 *
 * If speed is below off point, hold output to 0.
 * (Maybe if we get ambitious, add a FET to switch
 *  the fan's power entirely off below the OFF point)
 *
 * If the speed is above the full point, hold output to 1.
 *
 * If the speed is anywhere in between, configure
 * timer0 PWM output and set OCR0A to speed to get an
 * appropriate duty cycle.
 */
void set_fan_speed(uint8_t speed) {
	current_speed = speed;
	if (speed > FAN_FULL_POINT) {
		PORTB |= 1<<PB0;
		PORTB |= 1<<PB3;
		timer0_pwm_disable();
	} else if (speed < FAN_OFF_POINT) {
		PORTB &= ~(1<<PB0);
		PORTB &= ~(1<<PB3);
		timer0_pwm_disable();
	} else {
		timer0_pwm_enable();
		PORTB |= 1<<PB3;
		OCR0A = speed;
	}
}

int main(void) {
	sei(); /* Enable interrupts */
	
	/* Configure PB0 as output for fan PWM control */
	DDRB |= 1<<PB0;
	/* Configure PB3 as output for fan power control */
	DDRB |= 1<<PB3;
	PORTB |= 1<<PB3;
	
	/* Configure ADC - VCC = VREF, left adjust, in from PB4 */
	DDRB &= ~(1<<PB4);
	ADMUX = (0<<REFS0 | 1<<ADLAR | 2<<MUX0);
	/* Configure ADC - ADC Enable, prescale 128 for 62.5kHz rate */
	ADCSRA = (1<<ADEN | 7<<ADPS0);
	ADCSRA |= 1<<ADSC; /* Start ADC conversion */
	
	/* Configure timer1 to provide 60hz interrupt */
	TCCR1 = (1<<CTC1 | 11<<CS10);
	OCR1C = TIMER1_TICKS;
	OCR1A = TIMER1_TICKS;
	PLLCSR &= ~(1<<PCKE) & ~(1<<PLLE);
	TIMSK |= 1<<OCIE1A;
	
	set_fan_speed(0);
	
    set_sleep_mode(SLEEP_MODE_IDLE);
    while (1) {
		sleep_mode();
    }
}

